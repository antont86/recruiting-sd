package com.sevdesk.lite.invoice.mapper

import com.sevdesk.lite.invoice.model.CustomerModel
import com.sevdesk.lite.invoice.model.InvoiceModel
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.math.BigDecimal
import java.time.LocalDate
import java.time.OffsetDateTime

class InvoiceMapperServiceImplTest {

    private lateinit var invoiceMapperService: InvoiceMapperService

    @BeforeEach
    fun setUp() {
        invoiceMapperService = InvoiceMapperServiceImpl()
    }

    @Test
    fun should_map_invoice_model_to_invoice_with_status_OPEN() {
        val dueDate = LocalDate.now()
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val creationDate = OffsetDateTime.now()
        val invoiceModel = InvoiceModel(priceGross = BigDecimal("2.0"), priceNet = BigDecimal("1.0"), customer = customerModel, dueDate = dueDate, invoiceNumber = 1)

        Mockito.mockStatic(OffsetDateTime::class.java).use {
            it.`when`<Any>{ OffsetDateTime.now() }.thenReturn(creationDate)

            val invoice = invoiceMapperService.mapToInvoice(invoiceModel)

            assertEquals(BigDecimal("2.0"), invoice.priceGross)
            assertEquals(BigDecimal("1.0"), invoice.priceNet)
            assertEquals("Peter", invoice.customer?.surname)
            assertEquals("Parker", invoice.customer?.givenname)
            assertEquals("1", invoice.invoiceNumber)
            assertEquals(dueDate, invoice.dueDate)
            assertEquals("OPEN", invoice.status)
            assertEquals(creationDate, invoice.creationDate)
            assertEquals(BigDecimal("1"), invoice.quantity)
        }
    }
}