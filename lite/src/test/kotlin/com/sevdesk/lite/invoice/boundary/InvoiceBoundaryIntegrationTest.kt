package com.sevdesk.lite.invoice.boundary

import com.sevdesk.lite.invoice.control.InvoiceModelFacade
import com.sevdesk.lite.invoice.control.InvoiceRepository
import com.sevdesk.lite.invoice.control.InvoiceService
import com.sevdesk.lite.invoice.exception.InvoiceValidationException
import com.sevdesk.lite.invoice.exception.config.InvoiceBoundaryExceptionHandler
import com.sevdesk.lite.invoice.model.InvoiceModel
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import java.math.BigDecimal

@SpringBootTest
class InvoiceBoundaryIntegrationTest {
    private lateinit var mockMvc: MockMvc

    private lateinit var invoiceBoundary: InvoiceBoundary

    @Mock
    private lateinit var invoiceService: InvoiceService

    @Mock
    private lateinit var invoiceRepository: InvoiceRepository

    @Mock
    private lateinit var invoiceModelFacade: InvoiceModelFacade

    private val invoiceModelDto = """
            {
                "customer": {
                    "surname": "Peter",
                    "givenname": "Parker"
                },
                "priceNet": 200.23,
                "priceGross": 300.68,
                "dueDate": "2023-02-09",
                "status": "OPEN",
                "invoiceNumber": 1337,
                "quantity": 1
            }
        """.trimIndent()

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        invoiceBoundary = InvoiceBoundary(invoiceRepository, invoiceService, invoiceModelFacade)
        this.mockMvc = MockMvcBuilders.standaloneSetup(invoiceBoundary).setControllerAdvice(InvoiceBoundaryExceptionHandler()).build()

    }

    @Test
    fun should_expect_status_ok_and_saved_entity_as_response() {
        val invoiceModelCaptor = argumentCaptor<InvoiceModel>()


        mockMvc.perform(post("/invoices").content(invoiceModelDto).contentType(MediaType.APPLICATION_JSON))

        verify(invoiceModelFacade).addInvoiceModel(invoiceModelCaptor.capture())

        val capturedInvoiceModel = invoiceModelCaptor.firstValue
        assertEquals("Peter", capturedInvoiceModel.customer.surname)
        assertEquals("Parker", capturedInvoiceModel.customer.givenname)
        assertEquals(BigDecimal("200.23"), capturedInvoiceModel.priceNet)
        assertEquals(BigDecimal("300.68"), capturedInvoiceModel.priceGross)
        assertEquals(1337, capturedInvoiceModel.invoiceNumber)
    }

    @Test
    fun should_capture_InvoiceValidationException_and_return_bad_request_with_exception_message() {
        whenever(invoiceModelFacade.addInvoiceModel(any())).thenThrow(InvoiceValidationException("Validation error"))

        mockMvc.perform(post("/invoices").content(invoiceModelDto).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest)
                .andExpect(MockMvcResultMatchers.content().string("Validation error"))
    }

    @Test
    fun should_capture_any_other_exception() {
        whenever(invoiceModelFacade.addInvoiceModel(any())).thenThrow(RuntimeException("Validation error, sensible sql error for example"))

        mockMvc.perform(post("/invoices").content(invoiceModelDto).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError)
                .andExpect(MockMvcResultMatchers.content().string("Oops! Some error occurred"))
    }

}