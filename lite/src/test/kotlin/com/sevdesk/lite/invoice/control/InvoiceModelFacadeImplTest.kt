package com.sevdesk.lite.invoice.control

import com.sevdesk.lite.invoice.entity.Invoice
import com.sevdesk.lite.invoice.mapper.InvoiceMapperService
import com.sevdesk.lite.invoice.model.InvoiceModel
import com.sevdesk.lite.invoice.validator.InvoiceValidatorService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.inOrder
import org.mockito.kotlin.whenever
import java.lang.RuntimeException

class InvoiceModelFacadeImplTest {

    @Mock
    private lateinit var invoiceService: InvoiceService

    @Mock
    private lateinit var invoiceValidatorService: InvoiceValidatorService

    @Mock
    private lateinit var invoiceMapperService: InvoiceMapperService

    @Mock
    private lateinit var invoiceModel: InvoiceModel

    @Mock
    private lateinit var invoice: Invoice

    private lateinit var invoiceModelFacade: InvoiceModelFacade

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        invoiceModelFacade = InvoiceModelFacadeImpl(invoiceService, invoiceMapperService, invoiceValidatorService)
        whenever(invoiceService.saveInvoice(invoice)).thenReturn(invoice)
    }

    @Test
    fun should_validate_before_mapping_saving_invoice() {
        whenever(invoiceMapperService.mapToInvoice(invoiceModel)).thenReturn(invoice)

        invoiceModelFacade.addInvoiceModel(invoiceModel)

        val inOrder = inOrder(invoiceValidatorService, invoiceMapperService, invoiceService)

        inOrder.verify(invoiceValidatorService).validate(invoiceModel)
        inOrder.verify(invoiceMapperService).mapToInvoice(invoiceModel)
        inOrder.verify(invoiceService).saveInvoice(invoice)
    }

    @Test
    fun should_return_invoice_after_saving() {
        whenever(invoiceMapperService.mapToInvoice(invoiceModel)).thenReturn(invoice)

        val resultInvoice = invoiceModelFacade.addInvoiceModel(invoiceModel)

        assertEquals(invoice, resultInvoice)
    }

    @Test
    fun should_throw_exception_when_validation_throws_exception() {
        whenever(invoiceValidatorService.validate(any<InvoiceModel>())).thenThrow(RuntimeException("Some error"))

        assertThrows<Exception> {
            invoiceModelFacade.addInvoiceModel(invoiceModel)
        }
    }
}