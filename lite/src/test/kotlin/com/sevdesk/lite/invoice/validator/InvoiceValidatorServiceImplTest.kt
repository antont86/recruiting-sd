package com.sevdesk.lite.invoice.validator

import com.sevdesk.lite.invoice.exception.InvoiceValidationException
import com.sevdesk.lite.invoice.model.CustomerModel
import com.sevdesk.lite.invoice.model.InvoiceModel
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.math.BigDecimal
import java.time.LocalDate
import java.time.Period

class InvoiceValidatorServiceImplTest {

    private lateinit var invoiceValidatorService: InvoiceValidatorService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        invoiceValidatorService = InvoiceValidatorServiceImpl()
    }

    @Test
    fun should_throw_exception_if_set_due_date_is_in_the_past() {
        val dueDate = LocalDate.now()
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(1.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = dueDate.minus(Period.ofDays(1)), invoiceNumber = 1)

        Mockito.mockStatic(LocalDate::class.java).use {
            it.`when`<Any> { LocalDate.now() }.thenReturn(dueDate)
            val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }
            assertEquals("Due date is in the past", exception.message)
        }

    }

    @Test
    fun should_throw_exception_if_set_due_date_is_the_same_date_as_today() {
        val dueDate = LocalDate.now()
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")

        val invoiceModel = InvoiceModel(priceGross = BigDecimal(1.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = dueDate, invoiceNumber = 1)

        Mockito.mockStatic(LocalDate::class.java).use {
            it.`when`<Any> { LocalDate.now() }.thenReturn(dueDate)
            val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

            assertEquals("Due date is in the past", exception.message)
        }

    }

    @Test
    fun should_throw_exception_if_net_price_is_0() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(1.0.toString()), priceNet = BigDecimal(0.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Net price must be greater than 0", exception.message)
    }

    @Test
    fun should_throw_exception_if_net_price_is_smaller_than_0() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(1.0.toString()), priceNet = BigDecimal((-0.1).toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Net price must be greater than 0", exception.message)
    }

    @Test
    fun should_throw_exception_if_net_price_decimal_places_is_longer_than_2() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(1.0.toString()), priceNet = BigDecimal(0.132.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Decimal places of net price longer than 2 not allowed", exception.message)
    }

    @Test
    fun should_throw_exception_if_gross_price_is_0() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(0.0.toString()), priceNet = BigDecimal(2.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Gross price must be greater than 0", exception.message)
    }

    @Test
    fun should_throw_exception_if_gross_price_is_smaller_than_0() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal((-0.1).toString()), priceNet = BigDecimal(2.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Gross price must be greater than 0", exception.message)
    }

    @Test
    fun should_throw_exception_if_gross_price_is_smaller_than_net_price() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(1.99.toString()), priceNet = BigDecimal(2.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Gross price must be greater than net price", exception.message)
    }

    @Test
    fun should_not_throw_exception_if_gross_price_is_equal_to_net_price() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(1.99.toString()), priceNet = BigDecimal(1.99.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        assertDoesNotThrow { invoiceValidatorService.validate(invoiceModel) }
    }

    @Test
    fun should_throw_exception_if_gross_price_decimal_places_is_longer_than_2() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Parker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(1.002.toString()), priceNet = BigDecimal(0.13.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Decimal places of gross price longer than 2 not allowed", exception.message)
    }

    @Test
    fun should_throw_exception_if_name_is_empty() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "     ")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(2.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Name cannot be empty", exception.message)
    }

    @Test
    fun should_throw_exception_if_name_contains_special_characters() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Pe!${"$"}${"§"}\\%&/()=?´`+*'#-_.:;,<>^°€ter")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(2.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Letters a-z allowed in name only", exception.message)
    }

    @Test
    fun should_throw_exception_if_name_contains_numbers() {
        val customerModel = CustomerModel(surname = "Peter", givenname = "Pa8ker")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(2.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Letters a-z allowed in name only", exception.message)
    }

    @Test
    fun should_allow_space_in_name() {
        val customerModel = CustomerModel(surname = "Pe ter The", givenname = "Parker")

        val invoiceModel = InvoiceModel(priceGross = BigDecimal(2.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        assertDoesNotThrow { invoiceValidatorService.validate(invoiceModel) }
    }


    @Test
    fun should_throw_exception_if_surname_is_empty() {
        val customerModel = CustomerModel(surname = "       ", givenname = "Pe ter The")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(2.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Surname cannot be empty", exception.message)
    }

    @Test
    fun should_throw_exception_if_surname_contains_special_characters() {
        val customerModel = CustomerModel(surname = "Pe!${"$"}${"§"}\\%&/()=?´`+*'#-_.:;,<>^°€ter", givenname = "Pe ter The")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(2.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Letters a-z allowed in surname only", exception.message)
    }

    @Test
    fun should_allow_space_in_surname() {
        val customerModel = CustomerModel(surname = "Par ker", givenname = "Pe ter The")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(2.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = 1)

        assertDoesNotThrow { invoiceValidatorService.validate(invoiceModel) }
    }

    @Test
    fun should_throw_exception_if_invoiceNumber_is_negative() {
        val customerModel = CustomerModel(surname = "Par ker", givenname = "Pe ter The")
        val invoiceModel = InvoiceModel(priceGross = BigDecimal(2.0.toString()), priceNet = BigDecimal(1.0.toString()), customer = customerModel, dueDate = LocalDate.now().plus(Period.ofDays(1)), invoiceNumber = -1)

        val exception = assertThrows(InvoiceValidationException::class.java) { invoiceValidatorService.validate(invoiceModel) }

        assertEquals("Positive invoice number allowed only", exception.message)
    }
}