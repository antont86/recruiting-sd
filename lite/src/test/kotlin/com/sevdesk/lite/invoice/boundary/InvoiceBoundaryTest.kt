package com.sevdesk.lite.invoice.boundary

import com.sevdesk.lite.invoice.control.InvoiceModelFacade
import com.sevdesk.lite.invoice.control.InvoiceRepository
import com.sevdesk.lite.invoice.control.InvoiceService
import com.sevdesk.lite.invoice.entity.Invoice
import com.sevdesk.lite.invoice.model.InvoiceModel
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.springframework.http.HttpStatus
import java.lang.RuntimeException

class InvoiceBoundaryTest {

    private lateinit var invoiceBoundary: InvoiceBoundary

    @Mock
    private lateinit var invoiceRepository: InvoiceRepository

    @Mock
    private lateinit var invoiceService: InvoiceService

    @Mock
    private lateinit var invoiceModelFacade: InvoiceModelFacade

    @Mock
    private lateinit var invoice: InvoiceModel

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        invoiceBoundary = InvoiceBoundary(invoiceRepository, invoiceService, invoiceModelFacade)
    }

    @Test
    fun should_save_invoice() {
        invoiceBoundary.addInvoice(invoice)

        verify(invoiceModelFacade).addInvoiceModel(invoice)
    }

    @Test
    fun should_throw_exception_let_controller_advice_handle_them() {
        whenever(invoiceModelFacade.addInvoiceModel(any())).thenThrow(RuntimeException("Some error"))

        assertThrows(Exception::class.java) { invoiceBoundary.addInvoice(invoice) }
    }

    @Test
    fun should_return_status_ok() {
        val response = invoiceBoundary.addInvoice(mock())

        assertEquals(HttpStatus.OK, response.statusCode)
    }

    @Test
    fun should_return_invoice() {
        val invoiceMock = mock<Invoice>()
        whenever(invoiceModelFacade.addInvoiceModel(any())).thenReturn(invoiceMock)

        val response = invoiceBoundary.addInvoice(mock())

        assertEquals(invoiceMock, response.body)
    }
}