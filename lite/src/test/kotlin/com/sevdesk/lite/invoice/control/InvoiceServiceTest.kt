package com.sevdesk.lite.invoice.control

import com.sevdesk.lite.invoice.entity.Invoice
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever

class InvoiceServiceTest {

    private lateinit var invoiceService: InvoiceService

    @Mock
    private lateinit var invoiceRepository: InvoiceRepository

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        invoiceService = InvoiceService(invoiceRepository)
    }

    @Test
    fun should_return_invoice_after_saving_invoice() {
        val invoice = Invoice()
        whenever(invoiceRepository.save(any<Invoice>())).thenReturn(invoice)

        val resultInvoice = invoiceService.saveInvoice(invoice)

        assertEquals(invoice, resultInvoice)
    }
}