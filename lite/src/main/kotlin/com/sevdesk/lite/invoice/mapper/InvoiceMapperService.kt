package com.sevdesk.lite.invoice.mapper

import com.sevdesk.lite.invoice.entity.Invoice
import com.sevdesk.lite.invoice.model.InvoiceModel

interface InvoiceMapperService {
    fun mapToInvoice(invoiceModel: InvoiceModel): Invoice

}
