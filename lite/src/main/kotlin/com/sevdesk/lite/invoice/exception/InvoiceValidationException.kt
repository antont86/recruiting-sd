package com.sevdesk.lite.invoice.exception

class InvoiceValidationException(override val message: String): RuntimeException(message) {
}