package com.sevdesk.lite.invoice.model

import java.math.BigDecimal
import java.time.LocalDate

data class InvoiceModel(val dueDate: LocalDate, val priceNet: BigDecimal, val priceGross: BigDecimal, val customer: CustomerModel, val invoiceNumber: Long)

