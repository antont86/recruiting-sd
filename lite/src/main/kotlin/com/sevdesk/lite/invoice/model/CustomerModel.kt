package com.sevdesk.lite.invoice.model

data class CustomerModel(val surname: String, val givenname: String)
