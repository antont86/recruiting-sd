package com.sevdesk.lite.invoice.exception.config

import com.sevdesk.lite.invoice.exception.InvoiceValidationException
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class InvoiceBoundaryExceptionHandler {

    @ExceptionHandler(InvoiceValidationException::class)
    fun handleInvoiceValidationException(invoiceValidationException: InvoiceValidationException): ResponseEntity<String> {
        return ResponseEntity.badRequest().body(invoiceValidationException.message)
    }

    @ExceptionHandler(Exception::class)
    fun handleInvoiceValidationException(exception: Exception): ResponseEntity<String> {
        return ResponseEntity.internalServerError().body("Oops! Some error occurred")
    }
}