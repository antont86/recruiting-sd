package com.sevdesk.lite.invoice.control

import com.sevdesk.lite.invoice.entity.Invoice
import com.sevdesk.lite.invoice.model.InvoiceModel

interface InvoiceModelFacade {
    fun addInvoiceModel(invoiceModel: InvoiceModel): Invoice
}