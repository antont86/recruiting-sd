package com.sevdesk.lite.invoice.validator

import com.sevdesk.lite.invoice.exception.InvoiceValidationException
import com.sevdesk.lite.invoice.model.InvoiceModel
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalDate

@Service
class InvoiceValidatorServiceImpl : InvoiceValidatorService {
    override fun validate(invoiceModel: InvoiceModel) {
        val now = LocalDate.now()
        if (invoiceModel.dueDate.isEqual(now) || invoiceModel.dueDate.isBefore(now)) throw InvoiceValidationException("Due date is in the past")

        if (invoiceModel.priceNet <= BigDecimal(0.0)) throw InvoiceValidationException("Net price must be greater than 0")
        if (invoiceModel.priceNet.scale() > 2) throw InvoiceValidationException("Decimal places of net price longer than 2 not allowed")
        if (invoiceModel.priceGross <= BigDecimal(0.0)) throw InvoiceValidationException("Gross price must be greater than 0")
        if (invoiceModel.priceGross < invoiceModel.priceNet) throw InvoiceValidationException("Gross price must be greater than net price")
        if (invoiceModel.priceGross.scale() > 2) throw InvoiceValidationException("Decimal places of gross price longer than 2 not allowed")

        if (invoiceModel.customer.givenname.isBlank()) throw InvoiceValidationException("Name cannot be empty")
        if (!invoiceModel.customer.givenname.matches("""([A-Z][a-z]+)""".toRegex()) && !invoiceModel.customer.givenname.matches("""(([a-zA-Z])+( [a-zA-Z]+))+""".toRegex())) throw InvoiceValidationException("Letters a-z allowed in name only")

        if (invoiceModel.customer.surname.isBlank()) throw InvoiceValidationException("Surname cannot be empty")
        if (!invoiceModel.customer.surname.matches("""([A-Z][a-z]+)""".toRegex()) && !invoiceModel.customer.surname.matches("""(([a-zA-Z])+( [a-zA-Z]+))+""".toRegex())) throw InvoiceValidationException("Letters a-z allowed in surname only")
        if (invoiceModel.invoiceNumber < 0) throw InvoiceValidationException("Positive invoice number allowed only")
    }
}