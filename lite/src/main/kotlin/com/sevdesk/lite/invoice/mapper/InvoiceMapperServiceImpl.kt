package com.sevdesk.lite.invoice.mapper

import com.sevdesk.lite.invoice.entity.Customer
import com.sevdesk.lite.invoice.entity.Invoice
import com.sevdesk.lite.invoice.model.InvoiceModel
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.OffsetDateTime

@Service
class InvoiceMapperServiceImpl: InvoiceMapperService {
    override fun mapToInvoice(invoiceModel: InvoiceModel): Invoice {
        val invoice = Invoice()
        val customer = Customer()

        customer.givenname = invoiceModel.customer.givenname
        customer.surname = invoiceModel.customer.surname

        invoice.priceGross = invoiceModel.priceGross
        invoice.priceNet = invoiceModel.priceNet
        invoice.customer = customer
        invoice.invoiceNumber = invoiceModel.invoiceNumber.toString()
        invoice.dueDate = invoiceModel.dueDate
        invoice.status = "OPEN"
        invoice.creationDate = OffsetDateTime.now()
        invoice.quantity = BigDecimal("1")

        return invoice
    }
}