package com.sevdesk.lite.invoice.control

import com.sevdesk.lite.invoice.entity.Invoice
import com.sevdesk.lite.invoice.mapper.InvoiceMapperService
import com.sevdesk.lite.invoice.model.InvoiceModel
import com.sevdesk.lite.invoice.validator.InvoiceValidatorService
import org.springframework.stereotype.Service

@Service
class InvoiceModelFacadeImpl constructor(private val invoiceService: InvoiceService, private val invoiceMapperService: InvoiceMapperService, private val invoiceValidatorService: InvoiceValidatorService) : InvoiceModelFacade {
    override fun addInvoiceModel(invoiceModel: InvoiceModel): Invoice {
        invoiceValidatorService.validate(invoiceModel)
        val invoice = invoiceMapperService.mapToInvoice(invoiceModel)

        return invoiceService.saveInvoice(invoice)
    }
}