package com.sevdesk.lite.invoice.validator

import com.sevdesk.lite.invoice.model.InvoiceModel

interface InvoiceValidatorService {
    fun validate(invoiceModel: InvoiceModel)
}
